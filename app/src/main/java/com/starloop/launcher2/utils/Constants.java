package com.starloop.launcher2.utils;


public class Constants {
    public static final int TYPE_BANNER = 100;
    public static final int TYPE_MY_APPS = 101;
    public static final int TYPE_APP_RECOMMEND_BIG = 102;
    public static final int TYPE_APP_RECOMMEND_SMALL = 103;
    public static final int TYPE_LOCAL_APPS = 104;
}
