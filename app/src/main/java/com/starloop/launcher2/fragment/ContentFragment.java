package com.starloop.launcher2.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.leanback.widget.ArrayObjectAdapter;
import androidx.leanback.widget.HeaderItem;
import androidx.leanback.widget.ItemBridgeAdapter;
import androidx.leanback.widget.ListRow;
import androidx.leanback.widget.OnChildViewHolderSelectedListener;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.starloop.launcher2.R;
import com.starloop.launcher2.activity.MainActivity;
import com.starloop.launcher2.bean.Content;
import com.starloop.launcher2.bean.Footer;
import com.starloop.launcher2.content.ContentPresenterSelector;
import com.starloop.launcher2.presenter.TypeAppRecommendBigPresenter;
import com.starloop.launcher2.presenter.TypeAppRecommendSmallPresenter;
import com.starloop.launcher2.presenter.TypeBannerPresenter;
import com.starloop.launcher2.presenter.TypeLocalAppsPresenter;
import com.starloop.launcher2.presenter.TypeMyAppsPresenter;
import com.starloop.launcher2.utils.Constants;
import com.starloop.launcher2.utils.FontDisplayUtil;
import com.starloop.launcher2.utils.LocalJsonResolutionUtil;
import com.starloop.launcher2.widgets.TabVerticalGridView;

import java.util.List;


public class ContentFragment extends Fragment {
    private static final String TAG = "ContentFragment";
    private static final String MSG_BUNDLE_KEY_ADD_ITEM = "msgBundleKeyItem";
    private static final int MSG_ADD_ITEM = 100;
    private static final int MSG_REMOVE_LOADING = 101;
    private TabVerticalGridView mVerticalGridView;
    private MainActivity mActivity;
    private View mRootView;
    private Handler mHandler;
    private ProgressBar mPbLoading;
    private ArrayObjectAdapter mAdapter;

    private int mCurrentTabPosition;
    private String mCurrentTabCode = "0";

    @SuppressLint("HandlerLeak")
    private class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_ADD_ITEM:
                    Content content = msg.getData().getParcelable(MSG_BUNDLE_KEY_ADD_ITEM);
                    if (content == null) {
                        break;
                    }
                    List<Content.DataBean> dataBeans = content.getData();
                    for (int i = 0; i < dataBeans.size(); i++) {
                        Content.DataBean dataBean = dataBeans.get(i);
                        addItem(dataBean);
                    }
                    addFooter();
                    mPbLoading.setVisibility(View.GONE);
                    mVerticalGridView.setVisibility(View.VISIBLE);
                    break;
                case MSG_REMOVE_LOADING:
                    mPbLoading.setVisibility(View.GONE);
                    break;
                default:
                    break;
            }
        }
    }

    private final Thread thread = new Thread(new Runnable() {
        @Override
        public void run() {
            String json = null;
            if (mCurrentTabCode == null) {
                mHandler.sendEmptyMessage(MSG_REMOVE_LOADING);
                return;
            }
            FragmentActivity activity = getActivity();
            if (activity == null) {
                return;
            }
            Log.e(TAG, "mCurrentTabCode=" + mCurrentTabCode);
            switch (mCurrentTabCode) {
                case "0":
                    json = LocalJsonResolutionUtil.getJson(activity, "My.json");
                    break;
            }
            if (json == null) {
                return;
            }
            Content content = LocalJsonResolutionUtil.JsonToObject(json, Content.class);
            final Message msg = Message.obtain();
            msg.what = MSG_ADD_ITEM;
            Bundle b = new Bundle();
            b.putParcelable(MSG_BUNDLE_KEY_ADD_ITEM, content);
            msg.setData(b);
            //延迟1秒模拟加载数据过程
            mHandler.sendMessageDelayed(msg, 1000);
        }
    });


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mActivity = (MainActivity) context;
        mHandler = new MyHandler();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG + " pos:", "onCreate: ");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            mRootView = inflater.inflate(R.layout.fragment_content, container, false);
            initView();
            initListener();
        }
        return mRootView;
    }

    public static ContentFragment newInstance() {
        ContentFragment fragment = new ContentFragment();
        Bundle bundle = new Bundle();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }

    private void initListener() {
        mVerticalGridView.addOnScrollListener(onScrollListener);
        mVerticalGridView.addOnChildViewHolderSelectedListener(onSelectedListener);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        thread.interrupt();
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
        }
        if (mVerticalGridView != null) {
            mVerticalGridView.removeOnScrollListener(onScrollListener);
            mVerticalGridView.removeOnChildViewHolderSelectedListener(onSelectedListener);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Log.e(TAG, "setUserVisibleHint mCurrentTabPosition: " + mCurrentTabPosition
                + " isVisibleToUser:" + isVisibleToUser);
    }

    private void initView() {
        mPbLoading = mRootView.findViewById(R.id.pb_loading);
        mVerticalGridView = mRootView.findViewById(R.id.hg_content);

        mVerticalGridView.setVerticalSpacing(FontDisplayUtil.dip2px(mActivity, 24));

        ContentPresenterSelector presenterSelector = new ContentPresenterSelector();
        mAdapter = new ArrayObjectAdapter(presenterSelector);
        ItemBridgeAdapter itemBridgeAdapter = new ItemBridgeAdapter(mAdapter);
        mVerticalGridView.setAdapter(itemBridgeAdapter);
    }

    private void loadData() {
        mPbLoading.setVisibility(View.VISIBLE);
        mVerticalGridView.setVisibility(View.INVISIBLE);
        thread.start();
    }

    private void addItem(Content.DataBean dataBean) {
        switch (dataBean.getContentCode()) {
            case Constants.TYPE_BANNER:
                ArrayObjectAdapter arrayObjectAdapter = new ArrayObjectAdapter(new TypeBannerPresenter());
                List<Content.DataBean.WidgetsBean> listZero = dataBean.getWidgets();
                if (listZero != null && listZero.size() >= 1) {
                    listZero = listZero.subList(0, 1);
                }
                arrayObjectAdapter.addAll(0, listZero);
                ListRow listRow = new ListRow(arrayObjectAdapter);
                addWithTryCatch(listRow);
                break;
            case Constants.TYPE_LOCAL_APPS:
                ArrayObjectAdapter arrayObjectAdapterFour = new ArrayObjectAdapter(new TypeLocalAppsPresenter());
                List<Content.DataBean.WidgetsBean> listFour = dataBean.getWidgets();
                if (listFour == null) {
                    return;
                }
                if (listFour.size() > 8) {
                    listFour = listFour.subList(0, 8);
                }
                arrayObjectAdapterFour.addAll(0, listFour);
                HeaderItem headerItemFour = null;
                if (dataBean.getShowTitle()) {
                    headerItemFour = new HeaderItem(dataBean.getTitle());
                }
                ListRow listRowFour = new ListRow(headerItemFour, arrayObjectAdapterFour);
                addWithTryCatch(listRowFour);
                break;
            case Constants.TYPE_MY_APPS:
                ArrayObjectAdapter arrayObjectAdapterOne = new ArrayObjectAdapter(new TypeMyAppsPresenter());
                List<Content.DataBean.WidgetsBean> listOne = dataBean.getWidgets();
                if (listOne == null) {
                    return;
                }
                if (listOne.size() > 7) {
                    listOne = listOne.subList(0, 7);
                }
                arrayObjectAdapterOne.addAll(0, listOne);
                HeaderItem headerItem = null;
                if (dataBean.getShowTitle()) {
                    headerItem = new HeaderItem(dataBean.getTitle());
                }
                ListRow listRowOne = new ListRow(headerItem, arrayObjectAdapterOne);
                addWithTryCatch(listRowOne);

                break;
            case Constants.TYPE_APP_RECOMMEND_BIG:
                ArrayObjectAdapter arrayObjectAdapterTwo = new ArrayObjectAdapter(new TypeAppRecommendBigPresenter());
                List<Content.DataBean.WidgetsBean> listTwo = dataBean.getWidgets();
                if (listTwo == null) {
                    return;
                }
                if (listTwo.size() > 3) {
                    listTwo = listTwo.subList(0, 3);
                }
                arrayObjectAdapterTwo.addAll(0, listTwo);
                HeaderItem headerItemTwo = null;
                if (dataBean.getShowTitle()) {
                    headerItemTwo = new HeaderItem(dataBean.getTitle());
                }
                ListRow listRowTwo = new ListRow(headerItemTwo, arrayObjectAdapterTwo);
                addWithTryCatch(listRowTwo);

                break;
            case Constants.TYPE_APP_RECOMMEND_SMALL:
                ArrayObjectAdapter arrayObjectAdapterThree = new ArrayObjectAdapter(new TypeAppRecommendSmallPresenter());
                List<Content.DataBean.WidgetsBean> listThree = dataBean.getWidgets();
                if (listThree == null) {
                    return;
                }
                if (listThree.size() > 7) {
                    listThree = listThree.subList(0, 7);
                }
                arrayObjectAdapterThree.addAll(0, listThree);
                HeaderItem headerItemThree = null;
                if (dataBean.getShowTitle()) {
                    headerItemThree = new HeaderItem(dataBean.getTitle());
                }
                ListRow listRowThree = new ListRow(headerItemThree, arrayObjectAdapterThree);
                addWithTryCatch(listRowThree);

                break;

        }
    }

    private void addFooter() {
        addWithTryCatch(new Footer());
    }


    private final RecyclerView.OnScrollListener onScrollListener
            = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
            switch (newState) {
                case RecyclerView.SCROLL_STATE_DRAGGING:
                case RecyclerView.SCROLL_STATE_SETTLING:
                    Glide.with(mActivity).pauseRequests();
                    break;
                case RecyclerView.SCROLL_STATE_IDLE:
                    Glide.with(mActivity).resumeRequests();
            }
        }
    };

    private final OnChildViewHolderSelectedListener onSelectedListener
            = new OnChildViewHolderSelectedListener() {
        @Override
        public void onChildViewHolderSelected(RecyclerView parent,
                                              RecyclerView.ViewHolder child,
                                              int position, int subposition) {
            super.onChildViewHolderSelected(parent, child, position, subposition);
            Log.e(TAG, "onChildViewHolderSelected: " + position
            );
        }
    };

    private void addWithTryCatch(Object item) {
        try {
            if (!mVerticalGridView.isComputingLayout()) {
                mAdapter.add(item);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
