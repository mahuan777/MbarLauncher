package com.starloop.launcher2.activity;

import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.SuperscriptSpan;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.starloop.launcher2.R;
import com.starloop.launcher2.base.BaseActivity;
import com.starloop.launcher2.fragment.ContentFragment;

public class MainActivity extends BaseActivity {
    ImageView networkIcon;
    ImageView audioModeIcon;
    TextView tvTemperatureDegree;
    TextView tvTemperatureDetail;
    ImageView imgWeatherIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupView();
        filledFragment();
    }

    private void filledFragment() {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ContentFragment fragment = ContentFragment.newInstance();
        ft.replace(R.id.main_container, fragment);
        ft.commit();
    }

    private void setupView() {

        tvTemperatureDegree = this.findViewById(R.id.tv_temperature_degree);
        tvTemperatureDetail = this.findViewById(R.id.tv_temperature_detail);

        SpannableString spanWeather = new SpannableString("雷阵雨\n成都");
        spanWeather.setSpan(new AbsoluteSizeSpan(18, false), 0, 3, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spanWeather.setSpan(new AbsoluteSizeSpan(15, false), 4, 6, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spanWeather.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.gray_text_color)), 4, 6, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvTemperatureDetail.append(spanWeather);
        SpannableString spanDegree = new SpannableString("32。");
        spanDegree.setSpan(new SuperscriptSpan(), 2, 3, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvTemperatureDegree.append(spanDegree);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}


