package com.starloop.launcher2.widgets;

import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.ViewCompat;

public class ImgRecommendConstraintLayout extends ConstraintLayout implements View.OnFocusChangeListener {

    private ValueAnimator valueAnimator;

    public ImgRecommendConstraintLayout(Context context) {
        this(context, null);
    }

    public ImgRecommendConstraintLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ImgRecommendConstraintLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        setFocusable(true);
        setOnFocusChangeListener(this);
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (v != null) {
            //获取焦点时变化
            if (hasFocus) {
                ViewCompat.animate(v)
                        .scaleX(1.05f)
                        .scaleY(1.05f)
                        .translationZ(1)
                        .setDuration(500)
                        .start();
            } else {
                ViewCompat.animate(v)
                        .scaleX(1)
                        .scaleY(1)
                        .translationZ(1)
                        .setDuration(500)
                        .start();
            }
        }
    }
}
