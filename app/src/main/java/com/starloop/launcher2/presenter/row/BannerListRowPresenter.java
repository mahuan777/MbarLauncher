package com.starloop.launcher2.presenter.row;

import android.annotation.SuppressLint;

import androidx.leanback.widget.BaseOnItemViewClickedListener;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.RowPresenter;

import com.starloop.launcher2.base.BaseListRowPresenter;
import com.starloop.launcher2.bean.Content;


public class BannerListRowPresenter extends BaseListRowPresenter {
    private static final String TAG = "BannerListRowPresenter";

    @SuppressLint("RestrictedApi")
    @Override
    protected void initializeRowViewHolder(final RowPresenter.ViewHolder holder) {
        super.initializeRowViewHolder(holder);
        setOnItemViewClickedListener(new BaseOnItemViewClickedListener() {
            @Override
            public void onItemClicked(Presenter.ViewHolder itemViewHolder,
                                      Object item, RowPresenter.ViewHolder rowViewHolder, Object row) {
                if (item instanceof Content.DataBean.WidgetsBean) {
//                    tv.getContext().startActivity(new Intent(tv.getContext(), VideoDetailActivity.class));
                }
            }
        });

    }
}
