package com.starloop.launcher2.presenter;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.leanback.widget.Presenter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.starloop.launcher2.R;
import com.starloop.launcher2.bean.Content;

import java.io.InputStream;
import java.util.List;


public class TypeLocalAppsPresenter extends Presenter {
    private static final String TAG = "LocalAppsListRowPresenter";
    private Context mContext;
    private BitmapDrawable bitmapDrawable;
    private boolean isResourceFromAsset = false;

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        if (mContext == null) {
            mContext = parent.getContext();
        }
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_type_local_setup_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        if (item instanceof Content.DataBean.WidgetsBean) {
            ViewHolder vh = (ViewHolder) viewHolder;
            String picPath = null;
            List<String> urls = ((Content.DataBean.WidgetsBean) item).getUrls();
            if (urls.size() > 0) {
                picPath = urls.get(0);
            }
            String url = judyeResourceFromAsset(picPath);
            Glide.with(mContext)
                    .load(isResourceFromAsset ? bitmapDrawable : url)
                    .apply(new RequestOptions()
                            .fitCenter()
                            .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                            .placeholder(R.drawable.bg_shape_default))

                    .into(vh.image);
        }
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
    }

    /**
     * 判断资源
     *
     * @param url
     * @return
     */
    private String judyeResourceFromAsset(String url) {
        if (url.contains("assets")) {
            InputStream abpath = mContext.getClass().getResourceAsStream(url);
            bitmapDrawable = new BitmapDrawable(mContext.getResources(), abpath);
            isResourceFromAsset = true;
        } else {
            isResourceFromAsset = false;
        }
        return url;
    }

    public static class ViewHolder extends Presenter.ViewHolder {
        private final ImageView image;

        public ViewHolder(View view) {
            super(view);
            image = view.findViewById(R.id.iv_type_one_poster);
        }
    }

}