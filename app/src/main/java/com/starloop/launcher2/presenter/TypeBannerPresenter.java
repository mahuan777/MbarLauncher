package com.starloop.launcher2.presenter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.leanback.widget.Presenter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.starloop.launcher2.R;
import com.starloop.launcher2.bean.Content;
import com.starloop.launcher2.bean.CustomViewsInfo;
import com.starloop.launcher2.utils.FontDisplayUtil;
import com.stx.xhb.xbanner.XBanner;

import java.util.ArrayList;
import java.util.List;


public class TypeBannerPresenter extends Presenter {
    private final String TAG = "TypeBannerPresenter";
    private Context mContext;

    @Override
    public Presenter.ViewHolder onCreateViewHolder(ViewGroup parent) {
        if (mContext == null) {
            mContext = parent.getContext();
        }
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_type_bannner_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        if (item instanceof Content.DataBean.WidgetsBean) {
            ViewHolder vh = (ViewHolder) viewHolder;
            List<String> picPath = ((Content.DataBean.WidgetsBean) item).getUrls();
            initView(vh.banner, picPath);
        }
    }

//     "urls":["http://image12.m1905.cn/mapps/uploadfile/edu/2014/0815/thumb_0_800_400_2014081504081797626.jpg",
//             "http://img.juimg.com/tuku/yulantu/110520/2445-11052020525497.jpg",
//             "http://www.rjzxw.com/dats/twjc/7/11836/11836-1.jpg",
//             "https://n.sinaimg.cn/ent/transform/712/w630h882/20190824/d7f5-icqznha5072940.jpg",
//             "https://n.sinaimg.cn/ent/transform/712/w630h882/20190824/d7f5-icqznha5072940.jpg"]

    private void initView(XBanner banner, List<String> picPath) {
        List<CustomViewsInfo> data = new ArrayList<>();
        for (String s : picPath) {
            data.add(new CustomViewsInfo(s));
        }
//        data.add(new CustomViewsInfo("http://image12.m1905.cn/mapps/uploadfile/edu/2014/0815/thumb_0_800_400_2014081504081797626.jpg"));
//        data.add(new CustomViewsInfo("http://img.juimg.com/tuku/yulantu/110520/2445-11052020525497.jpg"));
//        data.add(new CustomViewsInfo("http://www.rjzxw.com/dats/twjc/7/11836/11836-1.jpg"));
//        data.add(new CustomViewsInfo("https://n.sinaimg.cn/ent/transform/712/w630h882/20190824/d7f5-icqznha5072940.jpg"));
//        data.add(new CustomViewsInfo("https://n.sinaimg.cn/ent/transform/712/w630h882/20190824/d7f5-icqznha5072940.jpg"));
        banner.setBannerData(R.layout.layout_custom_view, data);
        banner.loadImage(new XBanner.XBannerAdapter() {
            @Override
            public void loadBanner(XBanner banner, Object model, View view, int position) {
                ImageView imgView = (ImageView) view.findViewById(R.id.img_banner);
                Glide.with(mContext)
                        .load(((CustomViewsInfo) model).getXBannerUrl())
                        .apply(new RequestOptions()
                                .centerCrop()
                                .override(FontDisplayUtil.dip2px(mContext, 960),
                                        FontDisplayUtil.dip2px(mContext, 400))
                                .placeholder(R.drawable.bg_shape_default))
                        .into(imgView);

            }
        });
        banner.setOnItemClickListener(new XBanner.OnItemClickListener() {
            @Override
            public void onItemClick(XBanner banner, Object model, View view, int position) {
                Toast.makeText(mContext, "点击了" + position, Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
    }

    public static class ViewHolder extends Presenter.ViewHolder {
        private XBanner banner;

        public ViewHolder(View view) {
            super(view);
            banner = view.findViewById(R.id.banner);
        }
    }
}
