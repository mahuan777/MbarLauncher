package com.starloop.launcher2.presenter.row;

import android.annotation.SuppressLint;
import android.widget.Toast;

import androidx.leanback.widget.BaseOnItemViewClickedListener;
import androidx.leanback.widget.HorizontalGridView;
import androidx.leanback.widget.Presenter;
import androidx.leanback.widget.RowPresenter;

import com.starloop.launcher2.R;
import com.starloop.launcher2.base.BaseListRowPresenter;
import com.starloop.launcher2.bean.Content;
import com.starloop.launcher2.provider.InitProvider;


public class LocalAppsListRowPresenter extends BaseListRowPresenter {

    @SuppressLint("RestrictedApi")
    @Override
    protected void initializeRowViewHolder(RowPresenter.ViewHolder holder) {
        super.initializeRowViewHolder(holder);
        final ViewHolder rowViewHolder = (ViewHolder) holder;


        // 控制水平（左 上 右 下） 整体距离
        rowViewHolder.getGridView().setPadding(
                (int) InitProvider.mContext.getResources().getDimension(R.dimen.px_item_padding_left),
                0, 0, 0);
        // 控制水平 Item 间距
        rowViewHolder.getGridView().setHorizontalSpacing(
                (int) InitProvider.mContext.getResources().getDimension(R.dimen.px_item_padding_middle_local_apps));

        rowViewHolder.getGridView().setFocusScrollStrategy(HorizontalGridView.FOCUS_SCROLL_ITEM);

        setOnItemViewClickedListener(new BaseOnItemViewClickedListener() {
            @Override
            public void onItemClicked(Presenter.ViewHolder itemViewHolder,
                                      Object item, RowPresenter.ViewHolder rowViewHolder, Object row) {
                if (item instanceof Content.DataBean.WidgetsBean) {
                    Toast.makeText(((ViewHolder) rowViewHolder).getGridView().getContext(),
                            "位置:" + ((ViewHolder) rowViewHolder).getGridView().getSelectedPosition(),
                            Toast.LENGTH_SHORT).show();
//                    ((ViewHolder) rowViewHolder).getGridView().getContext().startActivity(new Intent(((ViewHolder) rowViewHolder).getGridView().getContext(), VideoDetailActivity.class));
                }
            }
        });
    }
}
