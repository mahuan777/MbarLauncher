package com.starloop.launcher2.content;

import androidx.leanback.widget.ListRow;

import com.starloop.launcher2.base.BasePresenterSelector;
import com.starloop.launcher2.bean.Footer;
import com.starloop.launcher2.presenter.TypeAppRecommendBigPresenter;
import com.starloop.launcher2.presenter.TypeAppRecommendSmallPresenter;
import com.starloop.launcher2.presenter.TypeBannerPresenter;
import com.starloop.launcher2.presenter.TypeFooterPresenter;
import com.starloop.launcher2.presenter.TypeLocalAppsPresenter;
import com.starloop.launcher2.presenter.TypeMyAppsPresenter;
import com.starloop.launcher2.presenter.row.AppsRecommendBiggerListRowPresenter;
import com.starloop.launcher2.presenter.row.AppsRecommendSmallListRowPresenter;
import com.starloop.launcher2.presenter.row.BannerListRowPresenter;
import com.starloop.launcher2.presenter.row.LocalAppsListRowPresenter;
import com.starloop.launcher2.presenter.row.MyAppsListRowPresenter;


public class ContentPresenterSelector extends BasePresenterSelector {
    public ContentPresenterSelector() {
        /**
         *  Banner 展示
         */
        BannerListRowPresenter listRowPresenter = new BannerListRowPresenter();
        listRowPresenter.setShadowEnabled(false);
        listRowPresenter.setSelectEffectEnabled(false);
        listRowPresenter.setKeepChildForeground(false);
        addClassPresenter(ListRow.class, listRowPresenter, TypeBannerPresenter.class);

        /**
         *  本地应用
         */
        LocalAppsListRowPresenter localAppsPresenter = new LocalAppsListRowPresenter();
        localAppsPresenter.setShadowEnabled(false);
        localAppsPresenter.setSelectEffectEnabled(false);
        localAppsPresenter.setKeepChildForeground(false);
        addClassPresenter(ListRow.class, localAppsPresenter, TypeLocalAppsPresenter.class);


        /**
         *  我的应用
         */
        MyAppsListRowPresenter myAppsPresenter = new MyAppsListRowPresenter();
        localAppsPresenter.setShadowEnabled(false);
        localAppsPresenter.setSelectEffectEnabled(false);
        localAppsPresenter.setKeepChildForeground(false);
        addClassPresenter(ListRow.class, myAppsPresenter, TypeMyAppsPresenter.class);

        /**
         *  应用推荐(大)
         */
        AppsRecommendBiggerListRowPresenter appsRecommendPresenter = new AppsRecommendBiggerListRowPresenter();
        localAppsPresenter.setShadowEnabled(false);
        localAppsPresenter.setSelectEffectEnabled(false);
        localAppsPresenter.setKeepChildForeground(false);
        addClassPresenter(ListRow.class, appsRecommendPresenter, TypeAppRecommendBigPresenter.class);


        /**
         *  应用推荐(小)
         */
        AppsRecommendSmallListRowPresenter appsRecommendSmallPresenter = new AppsRecommendSmallListRowPresenter();
        localAppsPresenter.setShadowEnabled(false);
        localAppsPresenter.setSelectEffectEnabled(false);
        localAppsPresenter.setKeepChildForeground(false);
        addClassPresenter(ListRow.class, appsRecommendSmallPresenter, TypeAppRecommendSmallPresenter.class);


        /**
         *  增加底部 增加空间
         */
        addClassPresenter(Footer.class, new TypeFooterPresenter());
    }

}
